import pytest
import os
import apache_beam as beam

from tempfile import NamedTemporaryFile
from backports.tempfile import TemporaryDirectory
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions


@pytest.fixture(scope='function')
def temp_file():
    with NamedTemporaryFile() as file:
        yield file.name


@pytest.fixture(scope='function')
def temp_dir():
    with TemporaryDirectory() as d:
        yield d


@pytest.fixture(scope='function')
def other_temp_dir():
    with TemporaryDirectory() as d:
        yield d


@pytest.fixture(scope='function')
def pipeline():
    options = PipelineOptions()
    setup_options = options.view_as(SetupOptions)
    setup_options.save_main_session = True
    setup_options.setup_file = os.path.join(os.getcwd(), 'setup.py')
    return beam.Pipeline(runner='direct', options=options)
