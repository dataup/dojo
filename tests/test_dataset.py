import pytest
import os
import json
import stored

from backports.tempfile import TemporaryDirectory

from dojo import Dataset


@pytest.fixture('function')
def sample_input():
    with TemporaryDirectory() as temp_dir:
        version_dir = os.path.join(temp_dir, '1')
        os.makedirs(version_dir)
        with open(os.path.join(version_dir, 'foo-1'), 'w') as file:
            file.write(json.dumps({'foo': 'bar'}))
        yield {
            'url': temp_dir
        }


@pytest.fixture('function')
def other_sample_input():
    with TemporaryDirectory() as temp_dir:
        version_dir = os.path.join(temp_dir, '1')
        os.makedirs(version_dir)
        with open(os.path.join(version_dir, 'foo-1'), 'w') as file:
            file.write(json.dumps({'foo': 'other'}))
        yield {
            'url': temp_dir
        }


@pytest.fixture('function')
def sample_output():
    with TemporaryDirectory() as temp_dir:
        yield {
            'url': temp_dir
        }


@pytest.fixture('function')
def other_sample_output():
    with TemporaryDirectory() as temp_dir:
        yield {
            'url': temp_dir
        }


def identity_from_first(inputs):
    return inputs[0]


def assert_output(dataset, output, expected_rows):
    actual = stored.list_files(output['url'])
    output_path = os.path.join(dataset.timestamp, 'output-00000-of-00001.json')
    expected = [output_path, ]
    assert actual == expected

    output_path = os.path.join(output['url'], output_path)
    with open(output_path, 'r') as output_file:
        actual = [json.loads(line) for line in output_file.readlines()]
    assert actual == expected_rows


def test_run_raises_not_implemented():
    dataset = Dataset()
    with pytest.raises(NotImplementedError):
        dataset.run()


def test_run_identity_with_lambda(sample_input, sample_output):
    dataset = Dataset()
    dataset.inputs = [sample_input, ]
    dataset.output = sample_output
    dataset.process = lambda inputs: inputs[0]
    dataset.run()
    assert_output(dataset, sample_output, [{'foo': 'bar'}])


def test_run_identity_with_method(sample_input, sample_output):
    dataset = Dataset()
    dataset.inputs = [sample_input, ]
    dataset.output = sample_output
    dataset.process = identity_from_first
    dataset.run()
    assert_output(dataset, sample_output, [{'foo': 'bar'}])
