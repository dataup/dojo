import pytest
import os
import json
import stored

from backports.tempfile import TemporaryDirectory

from dojo import Dojo


@pytest.fixture('function')
def sample_input():
    with TemporaryDirectory() as temp_dir:
        version_dir = os.path.join(temp_dir, '1')
        os.makedirs(version_dir)
        with open(os.path.join(version_dir, 'foo-1'), 'w') as file:
            file.write(json.dumps({'foo': 'bar'}))
        yield {
            'url': temp_dir
        }


def test_run(temp_dir, sample_input):
    dojo = Dojo()
    dojo.run('dojo.datasets.storage_keys.StorageKeysDataset', args=[temp_dir, ])
    actual = stored.list_files(temp_dir, relative=True)
    expected = []
    assert sorted(actual) == sorted(expected)


def test_run_with_error_handler(monkeypatch, temp_dir, sample_input):
    monkeypatch.setenv('DOJO_ERROR_HANDLER', 'dojo.errors.MockErrorHandler')
    dojo = Dojo()
    dojo.run('dojo.datasets.storage_keys.StorageKeysDataset', args=[temp_dir, ])
    assert dojo.error_handler.errors == []
    actual = stored.list_files(temp_dir, relative=True)
    expected = []
    assert sorted(actual) == sorted(expected)
